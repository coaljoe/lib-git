module kristallos.ga/lib/debug/extra

go 1.20

require (
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/y0ssar1an/q v1.0.10
)

require (
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
)
