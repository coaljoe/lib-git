package ecs

import "bitbucket.org/coaljoe/lib/sr"

// Entity

func (e *Entity) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		e.id,
		e.components,
		//e.componentRecords,
		e.tagsmap,
	), nil
}

func (e *Entity) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&e.id,
		&e.components,
		//&e.componentRecords,
		&e.tagsmap,
	)

	return nil
}

// Component

func (c *Component) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		c.EntityId,
	), nil
}

func (c *Component) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&c.EntityId,
	)

	return nil
}

// Component

/*
func (c *Component) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		c.derp,
	//c.entity,
	), nil
}

func (c *Component) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&c.derp,
	) //&c.entity,

	return nil
}
*/

// ComponentRecord

/*
func (cr *ComponentRecord) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		e.id,
	), nil
}

func (e *Entity) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&e.id,
		&e.components,
		&e.componentRecords,
	)

	return nil
}
*/
