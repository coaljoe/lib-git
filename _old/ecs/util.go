package ecs

import "fmt"

// XXX not persistent
//var componentEntityMap map[interface{}]*Entity

func init() {

}

// Get entity for given component.
func GetEntity(c interface{}) *Entity {
	if v, ok := defaultWorld.componentEntityMap[c]; ok {
		return v
	} else {
		fmt.Printf("k = %T (%p)\n", c, c)
		/*
			println(len(defaultWorld.componentEntityMap))
			for k, v := range defaultWorld.componentEntityMap {
				fmt.Printf("%T (%p) -> %T (%p)\n", k, k, v, v)
			}
		*/
		showComponentEntityMap()
		fmt.Printf("ecs: ERROR: can't find the entity for component %T (%p) \n", c, c)
		panic("entity not found")
	}
}

func showComponentEntityMap() {
	println("componentEntityMap:")
	println("len:", len(defaultWorld.componentEntityMap))
	for k, v := range defaultWorld.componentEntityMap {
		fmt.Printf("%T (%p) -> %T (%p)\n", k, k, v, v)
	}
	println("end")
}
