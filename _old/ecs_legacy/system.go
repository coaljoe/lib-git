package ecs

type SystemI interface {
	Type() string
}

type System struct {
	EntityMap map[int]*Entity
}

func NewSystem() *System {
	s := &System{}
	s.EntityMap = make(map[int]*Entity)
	return s
}

func (s System) Entities() []*Entity {
	list := make([]*Entity, len(s.EntityMap))
	i := 0
	for _, ent := range s.EntityMap {
		list[i] = ent
		i++
	}
	return list
}

func (s *System) AddEntity(en *Entity) {
	s.EntityMap[en.Id()] = en
}

func (s *System) RemoveEntity(en *Entity) {
	delete(s.EntityMap, en.Id())
}
