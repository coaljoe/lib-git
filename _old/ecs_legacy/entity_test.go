package ecs

import (
	"reflect"
	"testing"
)

var C1T = &C1{}
var C2T = &C2{}
var C3T = &C3{}

var C1TT = reflect.TypeOf(&C1{})
var C2TT = reflect.TypeOf(&C2{})
var C3TT = reflect.TypeOf(&C3{})

type C1 struct {
	*Component
	name string
}

func (c *C1) Type() string { return "C1" }

type C2 struct {
	*Component
	name string
}

func (c *C2) Type() string { return "C2" }

type C3 struct {
	*Component
	name string
}

func (c *C3) Type() string { return "C3" }

func newC1(en *Entity, name string) *C1 {
	c := &C1{name: name}
	c.Component = NewComponent(en, c)
	return c
}
func newC2(en *Entity, name string) *C2 {
	c := &C2{name: name}
	c.Component = NewComponent(en, c)
	return c
}
func newC3(en *Entity, name string) *C3 {
	c := &C3{name: name}
	c.Component = NewComponent(en, c)
	return c
}

func TestEntity(t *testing.T) {
	t.Log("TestEntity begin")
	e1 := NewEntity()
	if !(e1.Id() == 0) {
		t.Error("fail")
	}
	t.Log("TestEntity end")
}

/*
func TestEntityFast(t *testing.T) {
	t.Log("TestEntityFast begin")

	e1 := NewEntity()
	_ = newC1(e1, "test c1")
	_ = newC2(e1, "test c2")
	_ = newC3(e1, "test c3")

	c := e1._GetFast(C3TT).(*C3)
	println(c.name)

	t.Log("TestEntityFast end")
}
*/

func BenchmarkEntity(b *testing.B) {
	//b.Log("BenchmarktEntity begin")
	e1 := NewEntity()
	c1 := newC1(e1, "benchmark test c1")
	_ = newC2(e1, "benchmark test c2")
	c3 := newC3(e1, "benchmark test c3")
	//_ = newC1(e1, "benchmark test c4")
	//c4 := newC1(e1, "benchmark test c4")
	_ = c1
	_ = c3
	//_ = c4
	//e1.AddComponent(newC1())
	for n := 0; n < b.N; n++ {
		//_ = e1.GetComponent(c1).(*C1)
		//_ = e1.GetComponent(C1T).(*C1)
		_ = e1.GetComponent(C3T).(*C3)
		//_ = e1._GetFast(C3TT).(*C3)
		//_ = e1._GetFast2(C3T).(*C3)
		/*
			if !(e1.Id() == 1) {
				//b.Error("fail")
			}
		*/
	}
	//b.Log("BenchmarkEntity end")
}
