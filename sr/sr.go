package sr

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/WastedCode/serializer"
    "github.com/iancoleman/orderedmap"
	"log"
	"os"
	"reflect"
)

var logNfo = log.New(os.Stdout, "sr: ", 0)
var logErr = log.New(os.Stderr, "sr: ERR ", log.Lshortfile)
var DisableLog = false

// XXX fixme?
var nilMagic = "__SR_NIL_MAGIC__"

type DeserializeableI interface {
	OnDeserialize()
}

type SerializeableI interface {
	OnSerialize()
}

func callOnSerialize(v interface{}) {
	// XXX should be called before serialization?
	switch v.(type) {
	case SerializeableI:
		logNfo.Printf("calling OnSerialize() on type: %T\n", v)
		v.(SerializeableI).OnSerialize()
	}
}

func callOnDeserialize(v interface{}) {
	switch v.(type) {
	case DeserializeableI:
		logNfo.Printf("calling OnDeserialize() on type: %T\n", v)
		v.(DeserializeableI).OnDeserialize()
	}
}

// Attempt to serialize whole value.
func SerializeValueType(json_ bool, v interface{}) []byte {
	var err error
	var b []byte

	if forceEncodeTypeJson {
		json_ = true
	}

	if !json_ {
		b, err = serializer.SerializeToGob(v)
	} else {
		b, err = json.Marshal(v)
	}
	if err != nil {
		panic(err)
	}
	callOnSerialize(v)
	return b
}

func DeserializeValueType(json_ bool, b []byte, v interface{}) {
	//var v interface{}
	var err error

	if forceEncodeTypeJson {
		json_ = true
	}

	if !json_ {
		err = serializer.DeserializeFromGob(b, v)
	} else {
		err = json.Unmarshal(b, v)
	}
	if err != nil {
		panic(err)
	}
	callOnDeserialize(v)
	//return v
}

func SerializeValue(v interface{}) []byte {
	return SerializeValueType(false, v)
}

func DeserializeValue(b []byte, v interface{}) {
	DeserializeValueType(false, b, v)
}

func SerializeByFieldsKVType(json_ bool, obj interface{}, fv ...interface{}) []byte {
	// Check args
	for i := 0; i < len(fv); i += 2 {
		k := fv[i].(string)
		v := fv[i+1]

		fmt.Println("k:", k)
		fmt.Println("v:", v)

		//s := reflect.Indirect(reflect.ValueOf(v))
		s := reflect.Indirect(reflect.ValueOf(obj))
		println(s.String())
		//metric := s.FieldByName(k).Interface()
		//fmt.Println(metric)
		//f := s.FieldByName(k)
		f, z := s.Type().FieldByName(k)
		fmt.Println(f)
		fmt.Println(z)

		fmt.Println("f.Name", f.Name)

		if k != f.Name {
			panic("sr: bad key name: " + k)
		}

		/*
		st := s
		f2, _ := st.Type().FieldByName("id")
		fmt.Println(f2.Name)
		*/
		//panic(4)
	}
	println("sr: check args done")
	//panic(2)
	return serializeByFields(json_, true, fv...)
}

func SerializeByFieldsKV(obj interface{}, fv ...interface{}) []byte {
	return SerializeByFieldsKVType(false, obj, fv...)
}

func serializeByFields(json_ bool, kvMode bool, fv ...interface{}) []byte {
	var w *bytes.Buffer
	var enc *gob.Encoder
	var encJ *json.Encoder

	if forceEncodeTypeJson {
		json_ = true
	}

	if !json_ {
		w, enc = _gobNewEnc()
	} else {
		w, encJ = _jsonNewEnc()
		//encJ.SetIndent("", "  ")
		encJ.SetIndent("", "    ")
	}


	//kvData := make(map[string]interface{})
	kvData := orderedmap.New()

	//fmt.Println("fv:", fv)

	//for i, v := range fv {
	for i := 0; i < len(fv); i++ {
		var k string
		var v interface{}

		//fmt.Println("->", fv[i])
	
		if !kvMode {
			v = fv[i]
		} else {
			k = fv[i].(string)
			v = fv[i+1]
		}

	/*
	{

	xv := reflect.ValueOf(v)
	t := xv.Type()
	st := t.Elem()
	f, z := st.FieldByName("id")

	fmt.Println("t:", t)
	fmt.Println("t2:", t.Kind())
	fmt.Println("t3:", xv.Interface())
	//fmt.Println("t4:", st.Index(0))
	fmt.Println("f:", f)
	fmt.Println("z:", z)
	fmt.Println("zzz:", f.Name)

	{
		//subvalMetric := "Var1"
		subvalMetric := "id"

		//s := reflect.ValueOf(&v).Elem()
		//s := reflect.ValueOf(v).Elem()
		s := reflect.Indirect(reflect.ValueOf(v))
		println(s.String())
		println(s.String())
		metric := s.FieldByName(subvalMetric).Interface()
		fmt.Println(metric)

		//metric := s.Elem().FieldByName("id").Interface()
		//fmt.Println(metric)
	}


	panic(3)
	}
	*/

		/*

		//t := reflect.TypeOf(v)
		z := reflect.ValueOf(v)
		t := z.Type()
		fmt.Println("t:", t)
		fmt.Println("t2:", t.String())
		fmt.Println("t3:", reflect.PtrTo(t))
		fmt.Printf("t4: %#v\n", reflect.PtrTo(t))
		fmt.Printf("XXX %#v\n", v)
		fmt.Println("t5:", t.Field(0).Name)

		*/

		// Skip keys for gobs in kvmode
		if kvMode && !json_  && i % 2 != 0 {
			//println("sr: XXX skip")
			continue
		}

		if !DisableLog {
			if !kvMode {
				logNfo.Printf("storing: %#v %T\n", v, v)
			} else {
				logNfo.Printf("storing: %#v %T k=\"%s\"\n", v, v, k)
			}
		}

		if !json_ {
			//fmt.Println("storing:", b)
			val := reflect.ValueOf(v)
			//if !val.IsValid() {
			if val.Kind() == reflect.Ptr && val.IsNil() {
				fmt.Println("fixing nil")
				v = nilMagic
				//panic("derp")
			}
		}

		if !json_ {
			_gobEnc(enc, v)
		} else {
			if !kvMode {
				_jsonEnc(encJ, v)
			} else {
				//kvData[k] = v
				kvData.Set(k, v)
				//_jsonEncKV(encJ, k, v)
				//panic(2)
			}
		}

		// Finally
		if kvMode {
			i += 1
		}
	}

	// Encode kvData
	if json_ && kvMode {
		_jsonEnc(encJ, kvData)
	}

	return w.Bytes()
}

func SerializeByFieldsType(json_ bool, fv ...interface{}) []byte {
	return serializeByFields(json_, false, fv...)
}

func DeserializeByFieldsType(json_ bool, b []byte, fv ...interface{}) {
	var dec *gob.Decoder
	var decJ *json.Decoder

	if forceEncodeTypeJson {
		json_ = true
	}


	if !json_ {
		dec = _gobNewDec(b)
	} else {
		decJ = _jsonNewDec(b)
	}

	for _, v := range fv {
		if !DisableLog {
			logNfo.Printf("restoring: %#v %T\n", v, v)
		}

		var err error
		if !json_ {
			err = _gobDec(dec, v)
		} else {
			err = _jsonDec(decJ, v)
		}

		if err != nil {
			logErr.Println("ERROR:", err)
			v = nil
		}
		if !DisableLog {
			logNfo.Printf("restoring: %#v %T\n", v, v)
		}
		/*
			switch v.(type) {
			case *string:
				panic("derp")
			}
		*/
	}
}

func SerializeByFields(fv ...interface{}) []byte {
	return SerializeByFieldsType(false, fv...)
}

func DeserializeByFields(b []byte, fv ...interface{}) {
	DeserializeByFieldsType(false, b, fv...)
}

// Finalize serialization process. Fixme?
// XXX To use with DeserializeByFields?
func Finalize(v interface{}) {
	callOnDeserialize(v)
}

func Register(v interface{}) {
	gob.Register(v)
}

func _gobNewEnc() (*bytes.Buffer, *gob.Encoder) {
	w := new(bytes.Buffer)
	enc := gob.NewEncoder(w)
	return w, enc
}

func _gobNewDec(buf []byte) *gob.Decoder {
	r := bytes.NewBuffer(buf)
	dec := gob.NewDecoder(r)
	return dec
}

func _gobEnc(enc *gob.Encoder, v interface{}) {
	err := enc.Encode(v)
	if err != nil {
		panic(err)
	}
}

func _gobDec(dec *gob.Decoder, v interface{}) error {
	err := dec.Decode(v)
	if err != nil {
		//panic(err)
	}
	return err
}
